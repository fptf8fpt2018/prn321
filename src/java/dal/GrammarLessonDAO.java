/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Account;
import models.AccountType;
import models.Comment;
import models.GrammarLesson;

/**
 *
 * @author Admin
 */
public class GrammarLessonDAO extends BaseDAO<GrammarLesson> {
    
    public int getTotalRecord() {
        String sqlQueryText = "SELECT COUNT([id])\n"
                + "  FROM [dbo].[GrammarLesson]";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlQueryText);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(GrammarLessonDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    
    public List<GrammarLesson> getAllByPage(int start, int count) {
        List<GrammarLesson> grammarLessons = new ArrayList<>();
        String sqlQueryText = "DECLARE @PAGE_INDEX INT = ?;\n"
                + "DECLARE @PAGE_SIZE INT = ?;\n"
                + "                WITH [GrammarLessonInRange] AS (\n"
                + "                SELECT [id]\n"
                + "						  ,[name]\n"
                + "						  ,[image]\n"
                + "						  ,[content]\n"
                + "                      ,ROW_NUMBER() OVER(ORDER BY [id]) AS RowNumber\n"
                + "                  FROM [dbo].[GrammarLesson])\n"
                + "               SELECT * FROM [GrammarLessonInRange] \n"
                + "                 WHERE RowNumber BETWEEN ( (@PAGE_INDEX - 1) * @PAGE_SIZE + 1 ) AND (@PAGE_INDEX* @PAGE_SIZE)";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlQueryText);
            preparedStatement.setInt(1, start);
            preparedStatement.setInt(2, count);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.isBeforeFirst()) {
                while (resultSet.next()) {
                    GrammarLesson grammarLesson = new GrammarLesson();
                    grammarLesson.setId(resultSet.getInt("id"));
                    grammarLesson.setName(resultSet.getString("name"));
                    grammarLesson.setImage(resultSet.getString("image"));
                    grammarLesson.setContent(resultSet.getString("content"));
                    grammarLessons.add(grammarLesson);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(GrammarLessonDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return grammarLessons;
    }
    
    @Override
    public List<GrammarLesson> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public GrammarLesson getOne(int id) {
        String sqlQueryText = "SELECT  acc.[fullname]\n"
                + "		, at.[name] AS Positon\n"
                + "		,cmt.[content] \n"
                + "		,grm.[id] AS grammar_id\n"
                + "      ,grm.[name] AS grammar_name\n"
                + "      ,[image] AS grammar_image\n"
                + "      ,grm.[content] AS grammar_content\n"
                + "  FROM \n"
                + "  [dbo].[AccountType] AS at\n"
                + "  INNER JOIN \n"
                + "  [dbo].[Account] AS acc\n"
                + "  ON at.[id] = acc.account_type_id \n"
                + "  INNER JOIN \n"
                + "  [dbo].[Comment] AS cmt\n"
                + "  ON acc.[username] like  cmt.[username]\n"
                + "  RIGHT OUTER JOIN  \n"
                + "  [dbo].[GrammarLesson] AS grm\n"
                + "  ON grm.[id]  = cmt.[lesson_id]  \n"
                + "  WHERE grm.[id] = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlQueryText);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.isBeforeFirst()) {
                GrammarLesson grammarLesson = null;
                while (resultSet.next()) {
                    if (grammarLesson == null) {
                        grammarLesson = new GrammarLesson();
                        grammarLesson.setId(resultSet.getInt("grammar_id"));
                        grammarLesson.setName(resultSet.getString("grammar_name"));
                        grammarLesson.setImage(resultSet.getString("grammar_image"));
                        grammarLesson.setContent(resultSet.getString("grammar_content"));
                    }
                    if (resultSet.getString("content") != null) {
                        Comment comment = new Comment();
                        comment.setContent(resultSet.getString("content"));
                        comment.setGrammarLesson(grammarLesson);
                        Account account = new Account();
                        account.setFullname(resultSet.getString("fullname"));
                        AccountType accountType = new AccountType();
                        accountType.setName(resultSet.getString("Positon"));
                        account.setAccountType(accountType);
                        comment.setAccount(account);
                        grammarLesson.getComments().add(comment);
                    }
                    
                }
                return grammarLesson;
            }
        } catch (SQLException ex) {
            Logger.getLogger(GrammarLessonDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    @Override
    public boolean addOne(GrammarLesson model) {
        String sqlUpdateText = "INSERT INTO [dbo].[GrammarLesson]\n"
                + "           ([name]\n"
                + "           ,[image]\n"
                + "           ,[content])\n"
                + "     VALUES\n"
                + "           ( ?\n"
                + "           , ?\n"
                + "           , ? )";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlUpdateText);
            preparedStatement.setString(1, model.getName());
            preparedStatement.setString(2, "trieudd");
            preparedStatement.setString(3, model.getContent());
            return preparedStatement.executeUpdate() == 1;
        } catch (SQLException ex) {
            Logger.getLogger(GrammarLessonDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    @Override
    public boolean editOne(GrammarLesson model) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public boolean removeOne(int id) {
        String sqlUpdatText = "DELETE FROM [dbo].[GrammarLesson]\n"
                + "      WHERE [id] = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareCall(sqlUpdatText);
            preparedStatement.setInt(1, id);
            return preparedStatement.executeUpdate() == 1;
        } catch (SQLException ex) {
            Logger.getLogger(GrammarLessonDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return false;
    }
    
}
