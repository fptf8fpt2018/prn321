/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Slider;

/**
 *
 * @author Admin
 */
public class SliderDAO extends BaseDAO<Slider> {
    
    @Override
    public List<Slider> getAll() {
        List<Slider> sliders = new ArrayList<>();
        String sqlQueryText = "SELECT [id]\n"
                + "      ,[name]\n"
                + "      ,[content]\n"
                + "      ,[image]\n"
                + "  FROM [dbo].[Slider]";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlQueryText);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {                
                Slider slider = new Slider();
                slider.setId(resultSet.getInt("id"));
                slider.setName(resultSet.getString("name"));
                slider.setContent(resultSet.getString("content"));
                slider.setImage(resultSet.getString("image"));
                sliders.add(slider);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SliderDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sliders;
    }
    
    @Override
    public Slider getOne(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public boolean addOne(Slider model) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public boolean editOne(Slider model) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public boolean removeOne(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
