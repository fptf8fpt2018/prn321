/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Account;
import models.AccountType;
import models.Functionality;

/**
 *
 * @author Admin
 */
public class AccountDAO extends BaseDAO<Account> {

    public Account getOne(String username, String password) {
        String sqlQueryText = "SELECT [username]\n"
                + "      ,[fullname]\n"
                + "      ,[avatar]\n"
                + "      ,acc.[account_type_id]\n"
                + "      ,accType.[name] AS [account_type_name]\n"
                + "      ,func.[url] AS func_url\n"
                + "  FROM [dbo].[Account] as acc\n"
                + "  INNER JOIN [dbo].[AccountType] as accType\n"
                + "  ON acc.[account_type_id]   = accType.[id]\n"
                + "  INNER JOIN [dbo].[AccountType_Functionality] as accType_func\n"
                + "  ON  accType_func.[account_type_id] =  accType.[id] \n"
                + "  INNER JOIN [dbo].[Functionality] as func\n"
                + "  ON  func.[id] =  accType_func.[functionality_id] \n"
                + "  WHERE [username] = ? AND [password] = ?\n"
                + "  ORDER BY [username]";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlQueryText);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.isBeforeFirst()) {
                Account account = null;
                AccountType accountType = new AccountType();
                while (resultSet.next()) {
                    if (account == null) {
                        account = new Account();
                        account.setUsername(resultSet.getString("username"));
                        account.setFullname(resultSet.getString("fullname"));
                        account.setAvatar(resultSet.getString("avatar"));
                        accountType.setId(resultSet.getInt("account_type_id"));
                        accountType.setName(resultSet.getString("account_type_name"));
                        account.setAccountType(accountType);
                    }

                    Functionality functionality = new Functionality();
                    functionality.setUrl(resultSet.getString("func_url"));
                    accountType.getFunctionalities().add(functionality);
                }
                return account;
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return null;
    }

    @Override
    public List<Account> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Account getOne(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean addOne(Account model) {
        String sqlUpdateText = "INSERT INTO [dbo].[Account]\n"
                + "           ([username]\n"
                + "           ,[password]\n"
                + "           ,[fullname]\n"
                + "           ,[avatar]\n"
                + "           ,[account_type_id])\n"
                + "     VALUES\n"
                + "           ( ?\n"
                + "           , ?\n"
                + "           , ?\n"
                + "           , ?\n"
                + "           , ? )";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlUpdateText);
            preparedStatement.setString(1, model.getUsername());
            preparedStatement.setString(2, model.getPassword());
            preparedStatement.setString(3, model.getFullname());
            preparedStatement.setString(4, model.getAvatar());
            preparedStatement.setInt(5, model.getAccountType().getId());
            return preparedStatement.executeUpdate() == 1;
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public boolean editOne(Account model) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean removeOne(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
