/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.BaseModel;

/**
 *
 * @author Admin
 * @param <T>
 */
public abstract class BaseDAO<T extends BaseModel> extends DBContext {

    protected Connection connection;

    public BaseDAO() {
        try {
            connection = getConnection();
        } catch (Exception ex) {
            Logger.getLogger(BaseDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public abstract List<T> getAll();

    public abstract T getOne(int id);

    public abstract boolean addOne(T model);

    public abstract boolean editOne(T model);

    public abstract boolean removeOne(int id);

    protected void closeConnection() {
        try {
            if (connection.isClosed()) {
                connection.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(BaseDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
