/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Comment;

/**
 *
 * @author Admin
 */
public class CommentDAO extends BaseDAO<Comment> {

    @Override
    public List<Comment> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Comment getOne(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean addOne(Comment model) {
        String sqlUpdateText = "INSERT INTO [dbo].[Comment]\n"
                + "           ([content]\n"
                + "           ,[lesson_id]\n"
                + "           ,[username])\n"
                + "     VALUES\n"
                + "           ( ?\n"
                + "           , ?\n"
                + "           , ? )";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlUpdateText);
            preparedStatement.setString(1, model.getContent());
            preparedStatement.setInt(2, model.getGrammarLesson().getId());
            preparedStatement.setString(3, model.getAccount().getUsername());
            return preparedStatement.executeUpdate() == 1;
        } catch (SQLException ex) {
            Logger.getLogger(CommentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public boolean editOne(Comment model) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean removeOne(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
