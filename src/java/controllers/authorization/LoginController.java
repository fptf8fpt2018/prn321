/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.authorization;

import dal.AccountDAO;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.Account;

/**
 *
 * @author Admin
 */
public class LoginController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Account account = new Account();
        account.setUsername(request.getParameter("txtUsername"));
        account.setPassword(request.getParameter("txtPass"));
        account = (new AccountDAO()).getOne(account.getUsername(), account.getPassword());
        if (account != null) {
            request.getSession().setAttribute("account", account);
            
            String ReM = request.getParameter("chkReM");
            Cookie cookieUser = new Cookie("cookieUser", account.getUsername());
            Cookie cookieReM = new Cookie("cookieReM", ReM);
            if (ReM.equals("1")) {
                int secondsPerWeek = 60 * 60 * 24 * 7;
                cookieUser.setMaxAge(secondsPerWeek);
                cookieReM.setMaxAge(secondsPerWeek);
            } else {
                cookieUser.setMaxAge(0);
                cookieReM.setMaxAge(0);
            }
            response.addCookie(cookieReM);
            response.addCookie(cookieUser);
            
            response.setCharacterEncoding("UTF-8");
            response.getWriter().println("<script>alert('Welcome " + account.getFullname() + "');</script>");
            response.getWriter().println("<script>window.location.reload();</script>");
        } else {
            response.getWriter().println("<script>alert('Username or password incorrect !!');</script>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
