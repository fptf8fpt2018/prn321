/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.authorization;

import dal.AccountDAO;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.Account;
import models.AccountType;

/**
 *
 * @author Admin
 */
public class RegisterController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String user = request.getParameter("user");
        String pass = request.getParameter("pass");
        String fullname = request.getParameter("fullname");
        String avt = request.getParameter("avt");

        Account account = new Account();
        account.setUsername(request.getParameter("user"));
        account.setPassword(request.getParameter("pass"));
        account.setFullname(request.getParameter("fullname"));
        account.setAvatar(request.getParameter("avt"));
        AccountType accountType = new AccountType();
        accountType.setId(3); //user
        account.setAccountType(accountType);

        if ((new AccountDAO()).addOne(account)) {
            response.getWriter().println("<script>alert('Register successfully !!');</script>");
            response.getWriter().println("<script>$('#background_register').hide(600);$('#background_login').fadeIn(800);</script>");
        } else {
            response.getWriter().println("<script>alert('Register failed !!');</script>");
        }

//        File file = new File("D:\\ALL_SOURCES\\1. all_code cô vân anh và thầy sơn\\OnlineNihongoStudy\\web\\avatars\\" + user + "\\" + avt);
//        if (file.mkdir()) {
//
//        } else {
//
//        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
