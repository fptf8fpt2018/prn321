/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.admin.grammar;

import controllers.authorization.BaseReuiredAuthorization;
import dal.GrammarLessonDAO;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.GrammarLesson;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author Admin
 */
public class AddController extends BaseReuiredAuthorization {

    @Override
    public void processGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    @Override
    public void processPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        GrammarLesson grammarLesson = new GrammarLesson();
        grammarLesson.setName(request.getParameter("title"));
        grammarLesson.setContent(request.getParameter("content"));

        ServletContext context = request.getServletContext();
        final String ADDRESS = context.getRealPath("/images/grammar/lesson");
        final int MAX_MEMORY_SIZE = 1024 * 1024 * 3; // 3MB
        final int MAX_REQUEST_SIZE = 1024 * 1024 * 50; // 50MB

        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        if (isMultipart) { // must !
            response.getWriter().println("<script>alert('Not have enctype=\"multipart/form-data\"');</script>");
            response.getWriter().println("<script>window.location.href = \"add\"</script>");
            return;
        }

        DiskFileItemFactory factory = new DiskFileItemFactory();
        factory.setSizeThreshold(MAX_MEMORY_SIZE);
        factory.setRepository(new File(System.getProperty("java.io.tmpdir")));

        ServletFileUpload fileUpload = new ServletFileUpload(factory);
        fileUpload.setSizeMax(MAX_REQUEST_SIZE);

        try {
            List<FileItem> fileItems = fileUpload.parseRequest(request);
            Iterator<FileItem> iterator = fileItems.iterator();
            while (iterator.hasNext()) {
                FileItem fileItem = iterator.next();
                if (!fileItem.isFormField()) {
                    String filePath = ADDRESS + File.separator + fileItem.getName();
                    grammarLesson.setImage(filePath);

                    File file = new File(filePath);
                    if (file.exists()) {
                        response.getWriter().println("<script>alert('file name already exist, must rename unique pls');</script>");
                        response.getWriter().println("<script>window.location.href = \"add\"</script>");
                        return;
                    } else {
                        // success
                        fileItem.write(file);
                    }
                }
//                else {
//                    response.getWriter().println("<script>alert('is form field');</script>");
//                    response.getWriter().println("<script>window.location.href = \"add\"</script>");
//                    return;
//                }
            }
        } catch (FileUploadException ex) {
            Logger.getLogger(AddController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(AddController.class.getName()).log(Level.SEVERE, null, ex);
        }

        if ((new GrammarLessonDAO()).addOne(grammarLesson)) {
            response.getWriter().println("<script>alert('Add successful !!!');</script>");
        } else {
            response.getWriter().println("<script>alert('Add failed !!!');</script>");
        }
        response.getWriter().println("<script>window.location.replace(\"add\")</script>");
    }

    private void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/views/admin/grammar/add.jsp").forward(request, response);
    }

}
