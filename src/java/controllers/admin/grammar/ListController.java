/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.admin.grammar;

import controllers.authorization.BaseReuiredAuthorization;
import dal.GrammarLessonDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.GrammarLesson;

/**
 *
 * @author Admin
 */
public class ListController extends BaseReuiredAuthorization {

    @Override
    public void processGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    @Override
    public void processPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    private void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int pageIndex = request.getParameter("page") == null ? 1 : Integer.parseInt(request.getParameter("page"));
        int pageSize = Integer.parseInt(getServletContext().getInitParameter("pageSize"));
        List<GrammarLesson> grammarLessons = (new GrammarLessonDAO()).getAllByPage(pageIndex, pageSize);
        request.setAttribute("grammarLessons", grammarLessons);

        int totalModel = (new GrammarLessonDAO()).getTotalRecord();
        int pageCount = totalModel / pageSize;
        if (pageCount * pageSize < totalModel) {
            pageCount++;
        }
        request.setAttribute("pageCount", pageCount);
//        
        request.getRequestDispatcher("/views/admin/grammar/list.jsp").forward(request, response);
    }

}
