/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Admin
 */
public class AccountType extends BaseModel {

    private List<Functionality> functionalities;

    public List<Functionality> getFunctionalities() {
        if (functionalities == null) {
            functionalities = new ArrayList<>();
        }
        return functionalities;
    }

    public void setFunctionalities(List<Functionality> functionalities) {
        this.functionalities = functionalities;
    }
}
