/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author Admin
 */
public class Comment extends BaseModel {

    private String content;
    private GrammarLesson grammarLesson;
    private Account account;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public GrammarLesson getGrammarLesson() {
        if (grammarLesson == null) {
            grammarLesson = new GrammarLesson();
        }
        return grammarLesson;
    }

    public void setGrammarLesson(GrammarLesson grammarLesson) {
        this.grammarLesson = grammarLesson;
    }

    public Account getAccount() {
        if (account == null) {
            account = new Account();
        }
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

}
