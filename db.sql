USE [NihongoStudyOnline]
GO
/****** Object:  Table [dbo].[VocabLesson]    Script Date: 07/29/2020 14:36:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VocabLesson](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[image] [varchar](50) NOT NULL,
 CONSTRAINT [PK_VocabLesson] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Slider]    Script Date: 07/29/2020 14:36:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Slider](
	[id] [int] NOT NULL,
	[name] [text] NOT NULL,
	[content] [text] NOT NULL,
	[image] [varchar](50) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Slider] ([id], [name], [content], [image]) VALUES (1, N'Guide to listen and read TOEIC', N'We will provide for you the best and most useful knowledge', N'slide1.png')
INSERT [dbo].[Slider] ([id], [name], [content], [image]) VALUES (2, N'Give away various listening and reading excercise', N'We will throw good, meaningful instruction away for you', N'slide2.png')
INSERT [dbo].[Slider] ([id], [name], [content], [image]) VALUES (3, N'Free TOEIC test', N'After you got enough skill we will start undergo interesting, awesome excercies ', N'slide3.png')
/****** Object:  Table [dbo].[Exam]    Script Date: 07/29/2020 14:36:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Exam](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[image] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Exam] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AccountType]    Script Date: 07/29/2020 14:36:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AccountType](
	[id] [int] NOT NULL,
	[name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_AccountType] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[AccountType] ([id], [name]) VALUES (1, N'admin')
INSERT [dbo].[AccountType] ([id], [name]) VALUES (2, N'mod')
INSERT [dbo].[AccountType] ([id], [name]) VALUES (3, N'user')
/****** Object:  Table [dbo].[ListeningLesson]    Script Date: 07/29/2020 14:36:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ListeningLesson](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[image] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ListenLesson] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GrammarLesson]    Script Date: 07/29/2020 14:36:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GrammarLesson](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[image] [varchar](50) NOT NULL,
	[content] [text] NOT NULL,
 CONSTRAINT [PK_Grammar] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[GrammarLesson] ON
INSERT [dbo].[GrammarLesson] ([id], [name], [image], [content]) VALUES (1, N'a', N'icon-service1.png', N'aa')
INSERT [dbo].[GrammarLesson] ([id], [name], [image], [content]) VALUES (2, N'b', N'icon-service2.png', N'bb')
INSERT [dbo].[GrammarLesson] ([id], [name], [image], [content]) VALUES (8, N'123', N'icon-service3.png', N'123')
INSERT [dbo].[GrammarLesson] ([id], [name], [image], [content]) VALUES (9, N'123', N'icon-service4.png', N'**123**

123')
INSERT [dbo].[GrammarLesson] ([id], [name], [image], [content]) VALUES (10, N'123', N'icon-service5.png', N'123

**12312**')
SET IDENTITY_INSERT [dbo].[GrammarLesson] OFF
/****** Object:  Table [dbo].[Functionality]    Script Date: 07/29/2020 14:36:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Functionality](
	[id] [int] NOT NULL,
	[url] [varchar](550) NOT NULL,
 CONSTRAINT [PK_Functionality] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Functionality] ([id], [url]) VALUES (1, N'/admin')
INSERT [dbo].[Functionality] ([id], [url]) VALUES (2, N'/admin/grammar/list')
INSERT [dbo].[Functionality] ([id], [url]) VALUES (3, N'/list')
INSERT [dbo].[Functionality] ([id], [url]) VALUES (4, N'/admin/grammar/delete')
INSERT [dbo].[Functionality] ([id], [url]) VALUES (5, N'/admin/grammar/add')
/****** Object:  Table [dbo].[ReadingLesson]    Script Date: 07/29/2020 14:36:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ReadingLesson](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[image] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ReadingLesson] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ReadingLessonDetail]    Script Date: 07/29/2020 14:36:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ReadingLessonDetail](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[no] [int] NOT NULL,
	[paragraph] [text] NOT NULL,
	[question] [text] NOT NULL,
	[option1] [text] NOT NULL,
	[option2] [text] NOT NULL,
	[option3] [text] NOT NULL,
	[option4] [text] NOT NULL,
	[answer] [varchar](50) NOT NULL,
	[reading_lesson_id] [int] NOT NULL,
 CONSTRAINT [PK_ReadingLessonDetail] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AccountType_Functionality]    Script Date: 07/29/2020 14:36:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountType_Functionality](
	[account_type_id] [int] NOT NULL,
	[functionality_id] [int] NOT NULL,
 CONSTRAINT [PK_AccountType_Functionality] PRIMARY KEY CLUSTERED 
(
	[account_type_id] ASC,
	[functionality_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[AccountType_Functionality] ([account_type_id], [functionality_id]) VALUES (1, 1)
INSERT [dbo].[AccountType_Functionality] ([account_type_id], [functionality_id]) VALUES (1, 2)
INSERT [dbo].[AccountType_Functionality] ([account_type_id], [functionality_id]) VALUES (1, 3)
INSERT [dbo].[AccountType_Functionality] ([account_type_id], [functionality_id]) VALUES (1, 4)
INSERT [dbo].[AccountType_Functionality] ([account_type_id], [functionality_id]) VALUES (1, 5)
INSERT [dbo].[AccountType_Functionality] ([account_type_id], [functionality_id]) VALUES (2, 3)
INSERT [dbo].[AccountType_Functionality] ([account_type_id], [functionality_id]) VALUES (3, 3)
/****** Object:  Table [dbo].[ListeningLessonDetail]    Script Date: 07/29/2020 14:36:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ListeningLessonDetail](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[image_name] [varchar](50) NOT NULL,
	[audio_mp3] [varchar](50) NOT NULL,
	[audio_gg] [varchar](50) NOT NULL,
	[question] [text] NOT NULL,
	[option1] [text] NOT NULL,
	[option2] [text] NOT NULL,
	[option3] [text] NOT NULL,
	[option4] [text] NOT NULL,
	[answer] [text] NOT NULL,
	[listen_lesson_id] [int] NOT NULL,
 CONSTRAINT [PK_ListenLessonDetail] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Exam_Detail]    Script Date: 07/29/2020 14:36:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Exam_Detail](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[no] [int] NOT NULL,
	[image] [varchar](50) NOT NULL,
	[audio_mp3] [varchar](50) NOT NULL,
	[audio_gg] [varchar](50) NOT NULL,
	[paragraph] [text] NOT NULL,
	[question] [text] NOT NULL,
	[option1] [text] NOT NULL,
	[option2] [text] NOT NULL,
	[option3] [text] NOT NULL,
	[option4] [text] NOT NULL,
	[answer] [varchar](50) NOT NULL,
	[exam_id] [int] NOT NULL,
 CONSTRAINT [PK_Exam_Detail] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Account]    Script Date: 07/29/2020 14:36:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Account](
	[username] [varchar](50) NOT NULL,
	[password] [varchar](50) NOT NULL,
	[fullname] [nvarchar](50) NOT NULL,
	[avatar] [varchar](50) NOT NULL,
	[account_type_id] [int] NOT NULL,
 CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Account] ([username], [password], [fullname], [avatar], [account_type_id]) VALUES (N'trieudd', N'123123', N'Đỗ Đức Triệu', N'12412', 1)
INSERT [dbo].[Account] ([username], [password], [fullname], [avatar], [account_type_id]) VALUES (N'trieudd1', N'123123', N'Trieu', N'', 2)
INSERT [dbo].[Account] ([username], [password], [fullname], [avatar], [account_type_id]) VALUES (N'trieudd123', N'123456', N'trieudd123', N'', 2)
INSERT [dbo].[Account] ([username], [password], [fullname], [avatar], [account_type_id]) VALUES (N'trieudd2', N'trieudd2', N'trieudd2', N'', 3)
INSERT [dbo].[Account] ([username], [password], [fullname], [avatar], [account_type_id]) VALUES (N'trieudd3', N'trieudd3', N'trieudd3', N'', 3)
INSERT [dbo].[Account] ([username], [password], [fullname], [avatar], [account_type_id]) VALUES (N'trieuddd', N'123123', N'Triệu', N'', 3)
/****** Object:  Table [dbo].[VocabLessonDetail]    Script Date: 07/29/2020 14:36:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VocabLessonDetail](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[mean] [varchar](50) NOT NULL,
	[transcribe] [varchar](50) NOT NULL,
	[audio_mp3] [varchar](50) NOT NULL,
	[audio_gg] [varchar](50) NOT NULL,
	[vocab_lesson_id] [int] NOT NULL,
 CONSTRAINT [PK_VocabLessonDetailed] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Result]    Script Date: 07/29/2020 14:36:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Result](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[correct_ans_total] [int] NOT NULL,
	[incorrect_ans_total] [int] NOT NULL,
	[time] [datetime] NOT NULL,
	[exam_id] [int] NOT NULL,
	[username] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Result] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Comment]    Script Date: 07/29/2020 14:36:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Comment](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[content] [nvarchar](max) NOT NULL,
	[lesson_id] [int] NOT NULL,
	[username] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Comment] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Comment] ON
INSERT [dbo].[Comment] ([id], [content], [lesson_id], [username]) VALUES (9, N'omg...beautiful....omg', 1, N'trieudd')
INSERT [dbo].[Comment] ([id], [content], [lesson_id], [username]) VALUES (12, N'nice', 1, N'trieudd')
INSERT [dbo].[Comment] ([id], [content], [lesson_id], [username]) VALUES (17, N'123', 1, N'trieudd')
INSERT [dbo].[Comment] ([id], [content], [lesson_id], [username]) VALUES (18, N'hihi', 1, N'trieudd1')
INSERT [dbo].[Comment] ([id], [content], [lesson_id], [username]) VALUES (19, N'dep qua', 1, N'trieuddd')
SET IDENTITY_INSERT [dbo].[Comment] OFF
/****** Object:  ForeignKey [FK_Account_AccountType]    Script Date: 07/29/2020 14:36:14 ******/
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_AccountType] FOREIGN KEY([account_type_id])
REFERENCES [dbo].[AccountType] ([id])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_AccountType]
GO
/****** Object:  ForeignKey [FK_AccountType_Functionality_AccountType]    Script Date: 07/29/2020 14:36:14 ******/
ALTER TABLE [dbo].[AccountType_Functionality]  WITH CHECK ADD  CONSTRAINT [FK_AccountType_Functionality_AccountType] FOREIGN KEY([account_type_id])
REFERENCES [dbo].[AccountType] ([id])
GO
ALTER TABLE [dbo].[AccountType_Functionality] CHECK CONSTRAINT [FK_AccountType_Functionality_AccountType]
GO
/****** Object:  ForeignKey [FK_AccountType_Functionality_Functionality]    Script Date: 07/29/2020 14:36:14 ******/
ALTER TABLE [dbo].[AccountType_Functionality]  WITH CHECK ADD  CONSTRAINT [FK_AccountType_Functionality_Functionality] FOREIGN KEY([functionality_id])
REFERENCES [dbo].[Functionality] ([id])
GO
ALTER TABLE [dbo].[AccountType_Functionality] CHECK CONSTRAINT [FK_AccountType_Functionality_Functionality]
GO
/****** Object:  ForeignKey [FK_GrammarComment_Account]    Script Date: 07/29/2020 14:36:14 ******/
ALTER TABLE [dbo].[Comment]  WITH CHECK ADD  CONSTRAINT [FK_GrammarComment_Account] FOREIGN KEY([username])
REFERENCES [dbo].[Account] ([username])
GO
ALTER TABLE [dbo].[Comment] CHECK CONSTRAINT [FK_GrammarComment_Account]
GO
/****** Object:  ForeignKey [FK_GrammarComment_GrammarLesson]    Script Date: 07/29/2020 14:36:14 ******/
ALTER TABLE [dbo].[Comment]  WITH CHECK ADD  CONSTRAINT [FK_GrammarComment_GrammarLesson] FOREIGN KEY([lesson_id])
REFERENCES [dbo].[GrammarLesson] ([id])
GO
ALTER TABLE [dbo].[Comment] CHECK CONSTRAINT [FK_GrammarComment_GrammarLesson]
GO
/****** Object:  ForeignKey [FK_Exam_Detail_Exam]    Script Date: 07/29/2020 14:36:14 ******/
ALTER TABLE [dbo].[Exam_Detail]  WITH CHECK ADD  CONSTRAINT [FK_Exam_Detail_Exam] FOREIGN KEY([exam_id])
REFERENCES [dbo].[Exam] ([id])
GO
ALTER TABLE [dbo].[Exam_Detail] CHECK CONSTRAINT [FK_Exam_Detail_Exam]
GO
/****** Object:  ForeignKey [FK_ListenLessonDetail_ListenLesson]    Script Date: 07/29/2020 14:36:14 ******/
ALTER TABLE [dbo].[ListeningLessonDetail]  WITH CHECK ADD  CONSTRAINT [FK_ListenLessonDetail_ListenLesson] FOREIGN KEY([listen_lesson_id])
REFERENCES [dbo].[ListeningLesson] ([id])
GO
ALTER TABLE [dbo].[ListeningLessonDetail] CHECK CONSTRAINT [FK_ListenLessonDetail_ListenLesson]
GO
/****** Object:  ForeignKey [FK_ReadingLessonDetail_ReadingLesson]    Script Date: 07/29/2020 14:36:14 ******/
ALTER TABLE [dbo].[ReadingLessonDetail]  WITH CHECK ADD  CONSTRAINT [FK_ReadingLessonDetail_ReadingLesson] FOREIGN KEY([reading_lesson_id])
REFERENCES [dbo].[ReadingLesson] ([id])
GO
ALTER TABLE [dbo].[ReadingLessonDetail] CHECK CONSTRAINT [FK_ReadingLessonDetail_ReadingLesson]
GO
/****** Object:  ForeignKey [FK_Result_Account]    Script Date: 07/29/2020 14:36:14 ******/
ALTER TABLE [dbo].[Result]  WITH CHECK ADD  CONSTRAINT [FK_Result_Account] FOREIGN KEY([username])
REFERENCES [dbo].[Account] ([username])
GO
ALTER TABLE [dbo].[Result] CHECK CONSTRAINT [FK_Result_Account]
GO
/****** Object:  ForeignKey [FK_Result_Exam]    Script Date: 07/29/2020 14:36:14 ******/
ALTER TABLE [dbo].[Result]  WITH CHECK ADD  CONSTRAINT [FK_Result_Exam] FOREIGN KEY([exam_id])
REFERENCES [dbo].[Exam] ([id])
GO
ALTER TABLE [dbo].[Result] CHECK CONSTRAINT [FK_Result_Exam]
GO
/****** Object:  ForeignKey [FK_VocabLessonDetailed_VocabLesson]    Script Date: 07/29/2020 14:36:14 ******/
ALTER TABLE [dbo].[VocabLessonDetail]  WITH CHECK ADD  CONSTRAINT [FK_VocabLessonDetailed_VocabLesson] FOREIGN KEY([vocab_lesson_id])
REFERENCES [dbo].[VocabLesson] ([id])
GO
ALTER TABLE [dbo].[VocabLessonDetail] CHECK CONSTRAINT [FK_VocabLessonDetailed_VocabLesson]
GO
