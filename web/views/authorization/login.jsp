<%-- 
    Document   : login
    Created on : Jul 21, 2020, 4:32:28 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login Page</title>
        <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
        <link href="${pageContext.request.contextPath}/css/authorization/login.css" rel="stylesheet" type="text/css"/>

    </head>
    <body>


        <span id="notify_login">        </span>

        <div id="background_login">
            <div id="div_login" >
                <div class="main">
                    <!--div class="user">
                    </div-->
                    <div class="login" style="height: 100%">
                        <div class="inset">
                            <!-----start-main---->

                            <%
                                String cookieUser = "", cookieReM = "";
                                Cookie[] cookies = request.getCookies();
                                for (int i = 0; i < cookies.length; i++) {
                                    switch (cookies[i].getName()) {
                                        case "cookieUser":
                                            cookieUser = cookies[i].getValue();
                                            break;
                                        case "cookieReM":
                                            cookieReM = cookies[i].getValue();
                                            break;
                                    }
                                }
                            %>

                            <form>

                                <div>
                                    <span>Username</span>
                                    <span><input type="text" name="txtUsername" id="txtUsername" required="" value="<%=cookieUser%>" placeholder="Enter your username"></span>
                                </div>
                                <div>
                                    <span>Password</span>
                                    <span><input type="password" name="txtPass" id="txtPass" required="" placeholder="Enter your password" ></span>
                                </div>
                                <div>
                                    <input type="checkbox" name="chkReM" id="chkReM" <%=cookieReM.equals("1") ? " checked " : ""%>/><span>Remember me next time</span>
                                </div>

                                <div class="sign">
                                    <!--div class="submit">
                                        <input type="submit" onclick="" value="ADD" >
                                    </div-->
                                    <button type="button" id="btnLoginByAjax" value="ADD đê">Login</button>
                                    <button type="button" id="btnReturnReg" value="ADD đê">&rarr;</button>
                                    <span class="forget-pass">
                                        <a href="#" style="text-decoration: none" id="btnExit">Exit</a>
                                    </span>
                                    <div class="clear"> </div>
                                </div>
                            </form>
                            <br>
                        </div>
                    </div>
                    <!-----//end-main---->

                </div>
            </div>
        </div>
    </body>
</html>
