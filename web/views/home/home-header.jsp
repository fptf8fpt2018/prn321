<%-- 
    Document   : home-header
    Created on : Jul 23, 2020, 8:44:53 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <!--HEADER ROW-->
        <div id="header-row">
            <div class="container">
                <div class="row">
                    <!--LOGO-->
                    <div class="span3"><a class="brand" href="${pageContext.request.contextPath}/home"><img src="${pageContext.request.contextPath}/images/logo.png" height="64" width="64"></a></div>
                    <!-- /LOGO -->

                    <!-- MAIN NAVIGATION -->  
                    <div class="span9">
                        <div class="navbar  pull-right">
                            <div class="navbar-inner">
                                <a data-target=".navbar-responsive-collapse" data-toggle="collapse" class="btn btn-navbar"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></a>
                                <div class="nav-collapse collapse navbar-responsive-collapse">
                                    <ul class="nav">
                                        <li class="active"><a href="${pageContext.request.contextPath}/home">Home</a></li>

                                        <li><a href="contact.html">About</a></li>

                                        <li><a href="contact.html">Contact</a></li>

                                        <c:set var="account" value="${sessionScope.account}" />

                                        <c:if test="${account eq null}">
                                            <li><a href="#" id="login" >Sign up</a></li>
                                                <%@include file="../authorization/login.jsp" %>
                                            <script src="${pageContext.request.contextPath}/js/authorization/login.js" type="text/javascript"></script>

                                            <li><a href="#" id="register">Register</a></li>
                                                <%@include file="../authorization/register.jsp" %>
                                            <!--below use for reg and login-->
                                            <script type="text/JavaScript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js" ></script>
                                            <script src="${pageContext.request.contextPath}/js/authorization/register.js" type="text/javascript"></script>
                                        </c:if>


                                        <c:if test="${account ne null}">
                                            <li class="active dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    ${sessionScope.account.fullname}<b class="caret"></b>
                                                </a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="about.html">My account</a></li>
                                                        <c:if test="${account.accountType.id eq 1}">
                                                        <li>
                                                            <a href="admin">Admin</a>
                                                        </li>
                                                    </c:if>
                                                    <li><a href="about.html">Bookmark</a></li>
                                                    <li><a href="logout">Log-out</a></li>
                                                </ul>
                                            </li>
                                        </c:if>

                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- MAIN NAVIGATION -->  
                </div>
            </div>
        </div>
        <!-- /HEADER ROW -->
    </body>
</html>
