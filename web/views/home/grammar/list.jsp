<%-- 
    Document   : grammar
    Created on : Jul 28, 2020, 1:17:46 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>List grammar lesson - Page</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Bootstrap -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/bootstrap-responsive.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet"> 

        <!--Font-->
        <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600" rel="stylesheet" type="text/css">



        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]-->
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <!--[endif]-->

        <!-- Fav and touch icons -->
        <link rel="shortcut icon" href="ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png">




        <!-- SCRIPT 
        ============================================================-->  
        <script src="http://code.jquery.com/jquery.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
    </head>
    <body>
        <%@include file="../home-header.jsp" %>

        <div class="container">
            <!--PAGE TITLE-->
            <div class="row">
                <div class="span12">
                    <div class="page-header">
                        <h1>
                            Grammar
                        </h1>
                    </div>
                </div>
            </div>
            <!-- /. PAGE TITLE-->

            <div class="row">
                <c:forEach var="grammarLesson" items="${requestScope.grammarLessons}">
                    <div class="span6">
                        <div class="media">
                            <a href="#" class="pull-left"><img src="${pageContext.request.contextPath}/images/admin/grammar/lesson/${grammarLesson.image}" class="media-object" alt=""></a>
                            <div class="media-body">
                                <h4 class="media-heading">
                                    ${grammarLesson.name}
                                </h4> 
                                <p>
                                    <c:set var="content" value="${fn:replace(grammarLesson.content, '**', '<b>')}" />
                                    <c:out value="${content}" escapeXml="false" />  
                                </p>
                                <a href="lesson?id=${grammarLesson.id}" class="buttons" type="button">Read more ...</a>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>


        <!--Pagination-->
        <link href="${pageContext.request.contextPath}/css/admin/pagination/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <script src="${pageContext.request.contextPath}/js/admin/pagination/bootstrap.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/admin/pagination/jquery.js" type="text/javascript"></script>
        <ul class="pagination justify-content-center">
            <li class="page-item ${param.page eq 1 ? " disabled " : ""}"  ${param.page eq 1 ? "onclick=\"return false\"":""}>
                <a class="page-link" href="${pageContext.request.contextPath}/home/grammar/list?page=${param.page - 1}" tabindex="-1">Previous</a>
            </li>
            <!-- requestScope.pageCount -->
            <c:set var="pageCount" value="${pageCount}"></c:set>
            <c:forEach var="i" begin="1" end="${pageCount}" >
                <li class="page-item ${param.page eq i ? " disabled " : ""}" ${param.page eq i ? "onclick=\"return false\"":""}>
                    <a class="page-link" href="${pageContext.request.contextPath}/home/grammar/list?page=${i}">${i}</a>
                </li>
            </c:forEach>
            <li class="page-item ${param.page eq pageCount ? " disabled " : ""} "  ${param.page eq pageCount ? "onclick=\"return false\"":""}>
                <a class="page-link" href="${pageContext.request.contextPath}/home/grammar/list?page=${param.page + 1}">Next</a>
            </li>
        </ul>

        <%@include file="../home-footer.jsp" %>
    </body>
</html>
