/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

jQuery(document).ready(function ($) {
    $('#register').click(function () {
        $('#background_register').fadeIn(600);
    });
    function showAdd() {
        document.getElementById('background_register').style.display = 'block';
    }

    $('#btnRegisterByAjax').on('click', function () {
        //  var level = $(this).val();
        var level = validate() && $(this).val();
        if (level) {
            $.ajax({
                type: 'POST',
                url: 'register',
                data: {
                    user: $('#txtUsernameReg').val(),
                    pass: $('#txtPassReg').val(),
                    fullname: $('#txtFullnameReg').val(),
                    avt: $('#avtReg').val()
                },
                success: function (htmlresponse) {
                    $('#txtUsernameReg').val('');
                    $('#txtPassReg').val('');
                    $('#txtPassRegTwice').val('');
                    $('#txtFullnameReg').val('');
                    $('#avtReg').val('');
                    $('#notify_login').html(htmlresponse);
                },
                error: function (e) {
                    alert('some trouble occour, please return later !!! ' + e);
                }
            });
        } else {
        }
    });
    function validate() {
        var user = document.getElementById('txtUsernameReg').value;
        var pass = document.getElementById('txtPassReg').value;
        var repass = document.getElementById('txtPassRegTwice').value;
        var fullname = document.getElementById('txtFullnameReg').value;
        var avt = document.getElementById('avtReg').value;

        if (user === '') {
            alert('Your username not be blank !!!');
            return false;
        } else if (fullname === '') {
            alert('Your fullname not be blank !!!');
            return false;
        }
//        else if (avt === '') {
//            alert('your avatar must !!!');
//            return false;
//        } 
        else if (pass === '') {
            alert('Your password not be blank !!!');
            return false;
        } else if (pass.length < 6) {
            alert('Your password must 6 character !!!');
            return false;
        } else if (repass === '') {
            alert('Must confirm password !!!');
            return false;
        } else if (repass !== pass) {
            document.getElementById('txtPassRegTwice').value = '';
            alert('Re-password must be matching !!!');
            return false;
        }
        return true;
    }


    document.getElementById('txtPassRegTwice').oninput = function () {
        var pass = $('#txtPassReg').val();
        var repass = $('#txtPassRegTwice').val();
        if (pass !== repass) {
            $('#txtPassRegTwice')[0].setCustomValidity('Re-password must be matching.');
        } else {
            $('#txtPassRegTwice')[0].setCustomValidity('');
            //     $('#txtPassRegTwice').setCustomValidity('');
        }
    }
//    $('#txtPassRegTwice').on('input', function () {
//        var pass = $('#txtPassReg').val();
//        var repass = $('#txtPassRegTwice').val();
//        if (pass !== repass) {
//            $('#txtPassRegTwice').setCustomValidity('Re-password must be matching.');
//        }
//    });



    $('#btnExitReg').click(hideAdd);
    function hideAdd() {
        $('#background_register').fadeOut(800);
    }
    function hideAddd() {
        document.getElementById('background_register').style.display = 'none';
    }

    $('#btnReturnSignIn').click(function () {
        $('#background_register').slideUp(800);
        $('#background_login').slideDown(800);
    });
});
