/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

jQuery(document).ready(function ($) {
    $('#login').click(function () {
        $('#background_login').fadeIn(600);
    });
    function showAdd() {
        document.getElementById('background_login').style.display = 'block';
    }

    $('#btnLoginByAjax').on('click', function () {
        var level = validate() && $(this).val();
        if (level) {
            $.ajax({
                type: 'POST',
                url: 'login',
                data: {
                    txtUsername: $('#txtUsername').val(),
                    txtPass: $('#txtPass').val(),
                    chkReM: $('#chkReM').is(':checked') ? "1" : "0"
                },
                success: function (htmlresponse) {
//                    $('#txtUsername').val('');
                    $('#txtPass').val('');
//                    $('#chkReM').prop('checked', false);
                    $('#notify_login').html(htmlresponse);
                },
                error: function (e) {
                    alert('some trouble occour, please return later !!! ' + e);
                }
            });
        }
    });
    function validate() {
        var user = document.getElementById('txtUsername').value;
        var pass = document.getElementById('txtPass').value;
        if (user === '') {
            alert('Your username not be blank !!!');
            return false;
        } else if (pass === '') {
            alert('Your password not be blank !!!');
            return false;
        } else if (pass.length < 6) {
            document.getElementById('txtPass').value = '';
            alert('Username or password is incorrect !!!');
            return false;
        }
        return true;
    }

    $('#btnExit').click(hideAdd);
    function hideAdd() {
        $('#background_login').fadeOut(800);
    }
    function hideAddd() {
        document.getElementById('background_login').style.display = 'none';
    }

    $('#btnReturnReg').click(function () {
        $('#background_register').slideDown(800);
        $('#background_login').slideUp(800);
    });
});
