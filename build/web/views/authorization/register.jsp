<%-- 
    Document   : login
    Created on : Jul 21, 2020, 4:32:28 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Register Page</title>
        <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
        <link href="${pageContext.request.contextPath}/css/authorization/register.css" rel="stylesheet" type="text/css"/>

    </head>
    <body>
        <span id="notify_register">        </span>

        <div id="background_register">
            <div id="div_register" >
                <div class="main">
                    <!--div class="user">

                    </div -->
                    <div class="register" style="width: 110%; height: 100%">
                        <div class="inset">
                            <!-----start-main---->
                            <form>
                                <table>
                                    <tr>
                                        <td>
                                            <div>
                                                <span>Username</span>
                                                <span><input type="text" name="txtUsernameReg" id="txtUsernameReg" required="" placeholder="Enter your username"></span>
                                            </div>
                                        </td>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td>
                                            <div>
                                                <span>Password</span>
                                                <span><input type="password" name="txtPassReg" id="txtPassReg" required="" placeholder="Enter your password"></span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div>
                                                <span>Your full-name</span>
                                                <span><input type="text" name="txtFullnameReg" id="txtFullnameReg" required="" placeholder="Enter your fullname"></span>
                                            </div>
                                        </td>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td>
                                            <div>
                                                <span>Re-password</span>
                                                <span><input type="password" name="txtPassRegTwice" id="txtPassRegTwice" required="" placeholder="Re-enter your password"></span>
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan="3">
                                            <div>
                                                <span>Your avatar</span>
                                                <span><input type="file" name="avtReg" id="avtReg" required="" placeholder="Choose avatar of you"></span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr><td><br> </td></tr>
                                    <tr>
                                    <div class="sign">
                                        <!--div class="submit">
                                            <input type="submit" onclick="" value="ADD" >
                                        </div-->
                                        <td colspan="2">
                                            <button type="button" id="btnReturnSignIn" value="ADD đê">&larr;</button>
                                            <button type="button" id="btnRegisterByAjax" value="ADD đê">Register</button>
                                        </td>
                                        <td>
                                            <span class="forget-pass">
                                                <a href="#" style="text-decoration: none" id="btnExitReg">Exit</a>
                                            </span>
                                        </td>
                                        <div class="clear"> </div>
                                    </div>
                                    </tr>

                                </table>
                            </form>
                            <br>
                        </div>
                    </div>
                    <!-----//end-main---->

                </div>
            </div>
        </div>
    </body>
</html>
