<%-- 
    Document   : lesson
    Created on : Jul 28, 2020, 2:02:00 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Grammar lesson - Page</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Bootstrap -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/bootstrap-responsive.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet"> 

        <!--Font-->
        <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600" rel="stylesheet" type="text/css">



        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]-->
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <!--[endif]-->

        <!-- Fav and touch icons -->
        <link rel="shortcut icon" href="ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png">




        <!-- SCRIPT 
        ============================================================-->  
        <script src="http://code.jquery.com/jquery.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>


        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">


        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/bootstrap-responsive.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="js/html5shiv.js"></script>
        <![endif]-->

        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-57-precomposed.png">
        <link rel="shortcut icon" href="img/favicon.png">



        <!-- SCRIPT 
        ============================================================-->  
        <script src="http://code.jquery.com/jquery.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    </head>
    <body>
        <%@include file="../home-header.jsp" %>


        <div class="container">
            <!--PAGE TITLE-->

            <div class="row">
                <div class="span12">
                    <div class="page-header">
                        <h1>
                            Grammar lesson
                        </h1>
                    </div>
                </div>
            </div>

            <!-- /. PAGE TITLE-->



            <div class="row">


                <div class="span9">
                    <!--Blog Post-->
                    <div class="blog-post">
                        <h2>
                            ${requestScope.grammarLesson.name}
                        </h2>

                        <div class="postmetadata">
                            <ul>
                                <li>
                                    <i class="icon-user"></i> <a href="#">Author Name</a>
                                </li>

                                <li>
                                    <i class="icon-calendar"></i>September 20th, 2013
                                </li>

                                <li>
                                    <i class="icon-comment"></i> <a href="#">100 Comments</a>
                                </li>
                                <li>
                                    <i class="icon-tags"></i> <a href="#">Themes</a>, <a href="#">Template</a>
                                </li>
                            </ul>
                        </div>



                        <img src="img/img1-870x400.jpeg">
                        <p>${requestScope.grammarLesson.content}</p>	

                    </div>



                    <!--/.Blog Post-->



                    <!--Comments-->

                    <h2>Comments</h2>


                    <div class="comments">

                        <c:forEach var="comment" items="${requestScope.grammarLesson.comments}">
                            <div class="media">
                                <a href="#" class="pull-left"><img src="img/img-60x60.jpg" class="media-object" alt=""></a>
                                <div class="media-body">
                                    <h4 class="media-heading">
                                        ${comment.account.fullname} <code>${comment.account.accountType.name}</code>
                                    </h4> 
                                    ${comment.content}
                                </div>
                            </div>
                        </c:forEach>


                        <!--Post Comments-->

                        <h2>Leave a Message</h2>
                        <form id="formCmt" name="formCmt" method="POST" action="${pageContext.request.contextPath}/comment">
                            <textarea name="cmtContent" class="span8" rows="10" placeholder="Message" form="formCmt"></textarea>
                            <input type="hidden" name="id" value="${grammarLesson.id}" />

                            <button type="submit" class="btn btn-large btn-primary">Submit</button>
                        </form>

                        <!--/.Post Comments-->



                    </div>

                    <!--/.Comments-->






                </div>	



                <div class="span3">
                    <div class="side-bar">

                        <h3>Categories</h3>
                        <ul class="nav nav-list">
                            <li><a href="#">Web Design</a></li>
                            <li><a href="#">Typography</a></li>
                            <li><a href="#">Inspiration</a></li>
                            <li><a href="#">Business</a></li>
                        </ul>

                    </div>


                    <div class="side-bar">
                        <h3>Tags</h3>

                        <a href="#">cras</a>,
                        <a href="#">sit</a>,
                        <a href="#">amet</a>,
                        <a href="#">nibh</a>,
                        <a href="#">libero</a>,
                        <a href="#">gravida</a>,
                        <a href="#">nulla</a>
                    </div>


                    <div class="side-bar">
                        <h3>Recent Post</h3>

                        <ul class="recent-post">
                            <li><a href=""><strong>The standard chunk of Lorem Ipsum used since </strong></a>
                                <small><i class="icon-user"></i> <a href="#">Author Name</a>,  <i class="icon-calendar"></i>Jul 20th, 2013</small>
                            </li>				

                            <li><a href=""><strong>The standard chunk of Lorem Ipsum used since </strong></a>
                                <small><i class="icon-user"></i> <a href="#">Author Name</a>,  <i class="icon-calendar"></i>Jul 20th, 2013</small>
                            </li>

                            <li><a href=""><strong>The standard chunk of Lorem Ipsum used since </strong></a>
                                <small><i class="icon-user"></i> <a href="#">Author Name</a>,  <i class="icon-calendar"></i>Jul 20th, 2013</small>
                            </li>


                        </ul>






                    </div>


                </div>


                <!--==================-->
            </div>
        </div>

        <%@include file="../home-footer.jsp" %>
    </body>
</html>
