<%-- 
    Document   : home-footer
    Created on : Jul 23, 2020, 8:46:59 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <!--Footer
     ==========================-->

        <footer>
            <div class="container">
                <div class="row">
                    <code style="color: black"> <small>© 2020 • All Rights Reserved </small>
                        <br>
                        Designed by <i class="fa fa-love"></i><a href="https://www.facebook.com/fptf8fpt2018/">TrieuDD</a>
                    </code>
                </div>
                <div class="span6">
                    <div class="social pull-right">
                        <a href="#"><img src="${pageContext.request.contextPath}/img/social/googleplus.png" alt=""></a>
                        <a href="#"><img src="${pageContext.request.contextPath}/img/social/dribbble.png" alt=""></a>
                        <a href="#"><img src="${pageContext.request.contextPath}/img/social/twitter.png" alt=""></a>
                        <a href="#"><img src="${pageContext.request.contextPath}/img/social/dribbble.png" alt=""></a>
                        <a href="#"><img src="${pageContext.request.contextPath}/img/social/rss.png" alt=""></a>
                    </div>
                </div>
            </div>

        </footer>

        <!--/.Footer-->
    </body>
</html>
