<%-- 
    Document   : home
    Created on : Jul 21, 2020, 1:39:58 PM
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Home</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Bootstrap -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/bootstrap-responsive.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet"> 

        <!--Font-->
        <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600" rel="stylesheet" type="text/css">



        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]-->
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <!--[endif]-->

        <!-- Fav and touch icons -->
        <link rel="shortcut icon" href="ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png">




        <!-- SCRIPT 
        ============================================================-->  
        <script src="http://code.jquery.com/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>


    </head>
    <body>
        <%@include file="home-header.jsp" %>

        <div class="container">

            <!--Carousel
            ==================================================-->

            <div id="myCarousel" class="carousel slide">
                <div class="carousel-inner">

                    <c:forEach var="slider" items="${requestScope.sliders}">
                        <div class="${slider.id eq 1 ? " active " :""} item">
                            <div class="container">
                                <div class="row">

                                    <div class="span6">

                                        <div class="carousel-caption">
                                            <h1>${slider.name}</h1>
                                            <p class="lead">${slider.content}</p>
                                            <a class="btn btn-large btn-primary" href="#">Sign up today</a>
                                        </div>

                                    </div>

                                    <div class="span6"> <img src="images/${slider.image}" height="353" width="353"></div>

                                </div>
                            </div>
                        </div>

                        <!--div class="item">

                            <div class="container">
                                <div class="row">

                                    <div class="span6">

                                        <div class="carousel-caption">
                                            <h1>Example headline</h1>
                                            <p class="lead">Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                                            <a class="btn btn-large btn-primary" href="#">Sign up today</a>
                                        </div>

                                    </div>

                                    <div class="span6"> <img src="img/slide/slide2.jpg"></div>

                                </div>
                            </div>

                        </div -->

                    </c:forEach>

                </div>
                <!-- Carousel nav -->
                <a class="carousel-control left " href="#myCarousel" data-slide="prev"><i class="icon-chevron-left"></i></a>
                <a class="carousel-control right" href="#myCarousel" data-slide="next"><i class="icon-chevron-right"></i></a>
                <!-- /.Carousel nav -->

            </div>
            <!-- /Carousel -->



            <!-- Feature 
              ==============================================-->


            <div class="row feature-box">
                <div class="span12">
                    <h1><span>Let's get start it</span></h1> 
                    <h3><span>〈始めましょう〉 </span> </h3>
                </div>

                <div class="span4">
                    <img src="img/icon1_1.png">
                    <h2>Vocabulary, Grammar</h2>
                    <h3>〈語彙、文法〉</h3>
                    <p>
                        Provide instructions closely to the test 
                    </p>
                    <a href="#" id="vocabGrammarMoreLink">Read More →</a>
                </div>
                <!-- Modal box for Vocab, Grammar-->
                <style>
                    body {font-family: Arial, Helvetica, sans-serif;}

                    /* The Modal (background) */
                    .modal {
                        display: none; /* Hidden by default */
                        position: fixed; /* Stay in place */
                        z-index: 1; /* Sit on top */
                        padding-top: 100px; /* Location of the box */
                        left: 0;
                        top: 0;
                        width: 100%; /* Full width */
                        height: 100%; /* Full height */
                        overflow: auto; /* Enable scroll if needed */
                        background-color: rgb(0,0,0); /* Fallback color */
                        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
                        margin: 0;
                    }

                    /* Modal Content */
                    .modal-content {
                        background-color: #fefefe;
                        margin: auto;
                        padding: 50px;
                        border: 1px solid #888;
                        width: 20%;
                    }

                    /* The Close Button */
                    .close {
                        color: #aaaaaa;
                        float: right;
                        font-size: 28px;
                        font-weight: bold;
                    }

                    .close:hover,
                    .close:focus {
                        color: #000;
                        text-decoration: none;
                        cursor: pointer;
                    }
                </style>
                <div id="myModalVocabGrammar" class="modal">

                    <!-- Modal content -->
                    <div class="modal-content">
                        <span class="close">&times;</span>
                        <a href="${pageContext.request.contextPath}/home/grammar/list">
                            <div>
                                <img src="${pageContext.request.contextPath}/images/admin/grammar/download.jpg" alt="Grammar" title="Grammar" height="250" width="250"/>
                            </div>
                        </a> 
                        <hr>
                        <a href="#vocab">                     
                            <div>
                                <img src="${pageContext.request.contextPath}/images/admin/vocab/vocab.png" alt="Vocabulary" title="Vocabulary"  height="250" width="250"/>
                            </div>
                        </a>   
                    </div>

                </div>
                <script>
                    // Get the modal
                    var modal = document.getElementById("myModalVocabGrammar");

                    // Get the button that opens the modal
                    var btn = document.getElementById("vocabGrammarMoreLink");

                    // Get the <span> element that closes the modal
                    var span = document.getElementsByClassName("close")[0];

                    // When the user clicks the button, open the modal 
                    btn.onclick = function () {
                        modal.style.display = "block";
                    }

                    // When the user clicks on <span> (x), close the modal
                    span.onclick = function () {
                        modal.style.display = "none";
                    }

                    // When the user clicks anywhere outside of the modal, close it
                    window.onclick = function (event) {
                        if (event.target == modal) {
                            modal.style.display = "none";
                        }
                    }
                </script>
                <!-- End modal box-->


                <div class="span4" style="padding-top: 25px;">
                    <img src="img/icon2.jpg">
                    <h2>Reading, Listening exercise</h2>
                    <h3>〈読書、リスニング練習〉</h3>
                    <p>
                        We provide various exercises type like exam test
                    </p>   
                    <a href="#">Read More →</a>    
                </div>

                <div class="span4">
                    <img src="img/icon3_1.png">
                    <h2>Mock test</h2>
                    <h3>〈模擬試験〉</h3>
                    <p>
                        Let's try this amazing episode today or never after
                    </p>
                    <a href="#">Read More →</a>
                </div>
            </div>


            <!-- /.Feature -->

            <div class="hr-divider"></div>

            <!-- Row View -->


            <div class="row">
                <div class="span6"><img src="img/responsive.png"></div>

                <div class="span6">
                    <img class="hidden-phone" src="img/icon4.png" alt="">
                    <h1>Fully Responsive</h1>
                    <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
                    <a href="#">Read More →</a>
                </div>
            </div>


        </div>


        <!-- /.Row View -->

        <jsp:include page="home-footer.jsp" />
    </body>
</html>
